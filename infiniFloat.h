/*
	
	InfiniFloat class for C++
	Uses vectors to manipulate numbers of an infinite size (within resource limitations).
	Inspired by the BigDecimal and BigInteger classes which are native to Java.

	Copyright (c) 2016, Logan Matthew Nichols

	Permission to use, copy, modify, and/or distribute this software for any purpose with or 
	without fee is hereby granted, provided that the above copyright notice and this permission 
	notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS 
	SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE 
	AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
	NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR 
	PERFORMANCE OF THIS SOFTWARE.

*/

#include <iostream>
#include <vector>
#include <string>

using namespace std;

class InfiniFloat {
	
	private:
		unsigned long deciPoint;
		bool sign;
		vector<short> n;
		void trim();
		void flipSign();
		static void align(InfiniFloat& a, InfiniFloat& b);
		static vector<short> vectorize(long n);
		static vector<short> vectorize(string n);
	
	public:
		InfiniFloat();
		InfiniFloat(long input);
		InfiniFloat(string input);
		bool isPositive();
		bool isNegative();
		static void swap(InfiniFloat& a, InfiniFloat& b);
		static void swap(InfiniFloat* a, InfiniFloat* b);
		static InfiniFloat zero();
		static InfiniFloat one();
		
	friend ostream& operator << (ostream& outputStream, InfiniFloat& input) {
		
		if (!input.sign) {
			outputStream << "-";
		}

		for (long i = 0; i < (long)input.n.size(); i++) {
			outputStream 
				<< input.n[i] 
				<< ((i == input.deciPoint - 1 && i != (long)input.n.size() - 1) ? "." : "");
		}

		return outputStream;

	}

	friend istream& operator >> (istream& inputStream, InfiniFloat& num) {
		
		string input;
		inputStream >> input;
		num = *(new InfiniFloat(input));
		
		return inputStream;

	}

	friend InfiniFloat operator + (InfiniFloat a, InfiniFloat b) {

		// returns a + b

		//cout << "add" << endl;

		if (a.isPositive() && b.isNegative()) {
			b.flipSign();
			return a - b;
		} else if (a.isNegative() && b.isPositive()) {
			a.flipSign();
			return b - a;
		}

		//cout << "add2" << endl;

		InfiniFloat c;

		c.sign = a.isPositive() && b.isPositive();

		short carry = 0;
		short base = 10;	// base of the number system, 10 for decimal/base10
		int partial;
		long length = ((long)a.n.size() >= (long)b.n.size()) ? (long)a.n.size() : (long)b.n.size();

		align(a, b);

		//cout << "a: " << a << " | b: " << b << endl;

		for (long i = 1; i <= length; i++) {
			/*cout 
				<< "i = " << i
				<< " | a(" << ((long)a.n.size() - i) << ") = " << a.n[(long)a.n.size() - i] 
				<< " | b(" << ((long)b.n.size() - i) << ") = " << b.n[(long)b.n.size() - i] 
				<< " | carry = " << carry 
				<< " | partial = " << c
				<< endl;*/
			partial = a.n[(long)a.n.size() - i] + b.n[(long)b.n.size() - i] + carry;
			c.n.insert(c.n.begin(), partial % base);
			carry = partial / base;
		}

		c.deciPoint = a.deciPoint;

		if (carry > 0) {
			c.n.insert(c.n.begin(), carry);
			c.deciPoint++;
		}

		//cout << "add3" << endl;

		a.trim();
		b.trim();

		return c;

	}

	friend InfiniFloat operator - (InfiniFloat a, InfiniFloat b) {

		// returns a - b

		if (a.isPositive() && b.isNegative()) {
			b.flipSign();
			return a + b;
		} else if (a.isNegative() && b.isPositive()) {
			b.flipSign();
			return a + b;
		} else if (a.isNegative() && b.isNegative()) {
			a.flipSign();
			b.flipSign();
			swap(a, b);
		}
		
		InfiniFloat c;
		c.sign = true;

		if (a == b) {
			return InfiniFloat::zero();
		} else if (a < b) {
			//cout << "invert" << endl;
			swap(a, b);
			c.sign = false;
		}
		align(a, b);
		long aSize = (long)a.n.size();
		long bSize = (long)b.n.size();
		long length = (aSize >= bSize) ? aSize : bSize;

		for (long i = 1; i <= length; i++) {
			/*cout 
				<< "i: " << i
				<< " | aSize: " << aSize
				<< " | bSize: " << bSize
				<< " | currA = " << a.n[aSize - i] 
				<< " | currB = " << ((bSize - i < 0) ? 0 : b.n[bSize - i]);*/
			if (a.n[aSize - i] < ((bSize - i < 0) ? 0 : b.n[bSize - i])) {
				//cout << " | sub ";
				a.n[aSize - i] += 10;
				a.n[aSize - 1 - i]--;
			}
			c.n.insert(c.n.begin(), abs(a.n[aSize - i] - b.n[bSize - i]));
			//cout << endl;
		}

		c.deciPoint = a.deciPoint;
		//a.trim();
		//b.trim();
		c.trim();
		
		return c;

	}

	friend InfiniFloat operator * (InfiniFloat& a, InfiniFloat& b) {

		// returns a * b

		if (a == InfiniFloat::zero() || b == InfiniFloat::zero()) {
			return InfiniFloat::zero();
		}

		InfiniFloat c = 0;
		InfiniFloat partial;
		short temp;
		short carry = 0;

		for (long i = (long)b.n.size() - 1; i >= 0; i--) {
			for (long j = (long)a.n.size() - 1; j >= 0; j--) {
				temp = (a.n[j] * b.n[i]) + carry;
				carry = temp / 10;
				//cout << "pre-partial: " << partial << endl;
				partial.n.insert(partial.n.begin(), temp % 10);
				partial.deciPoint++;
				/*cout 
					<< "i: " << i 
					<< " | j: " << j 
					<< " | temp: " << temp
					<< " | carry: " << carry
					<< " | inserted: " << temp % 10
					<< " | partial: " << partial
					<< " | c = " << c
					<< endl;*/
			}
			if (carry > 0) {
				partial.n.insert(partial.n.begin(), carry);
				partial.deciPoint++;
				carry = 0;
			}
			/*cout 
				<< "1"
				<< " | c.deciPoint = " << c.deciPoint
				<< " | partial.deciPoint = " << partial.deciPoint
				<< endl;*/
			c = c + partial;
			//cout << "2";
			partial.n.erase(partial.n.begin(), partial.n.end());
			partial.deciPoint = 0;
			for (long k = 0; k < (long)b.n.size() - i; k++) {
				partial.n.push_back(0);
				partial.deciPoint++;	
			}
		}

		//cout << "c.deciPoint: " << c.deciPoint << endl;
		//c.deciPoint = (long)c.n.size() - 1;
		//cout << "c.deciPoint: " << c.deciPoint << endl;
		//c.trim();

		//cout << "a " << a.deciPoint << " | b " << b.deciPoint << endl;

		c.sign = (a.isPositive() && b.isPositive()) || (a.isNegative() && b.isNegative());
		c.deciPoint = 
			(long)c.n.size() 
			- (((long)a.n.size() - a.deciPoint) + ((long)b.n.size() - b.deciPoint));

		return c;

	}

	friend InfiniFloat operator / (InfiniFloat& a, InfiniFloat& b) {
		
		// returns a / b

		InfiniFloat quotient = 0;
		InfiniFloat dividend = a;
		InfiniFloat divisor = b;
		unsigned long aSize = (unsigned long)a.n.size();
		unsigned long bSize = (unsigned long)b.n.size();

		for (unsigned long i = 0; i < aSize; i++) {
			
		}

		return quotient;

	}

	// prefix increment
	friend InfiniFloat operator ++ (InfiniFloat& a) {
		a = a + InfiniFloat::one();
		return a;
	}

	// postfix increment
	friend InfiniFloat operator ++ (InfiniFloat& a, int) {
		return ++a;
	}

	// prefix decrement
	friend InfiniFloat operator -- (InfiniFloat& a) {
		a = a - InfiniFloat::one();
		return a;
	}

	// postfix decrement
	friend InfiniFloat operator -- (InfiniFloat& a, int) {
		return --a;
	}

	friend bool operator == (InfiniFloat& a, InfiniFloat& b) {
		return 
			a.n.size() == b.n.size() 
			&& a.deciPoint == b.deciPoint 
			&& a.sign == b.sign 
			&& a.n == b.n;
	}

	friend bool operator != (InfiniFloat& a, InfiniFloat& b) {
		return 
			a.n.size() != b.n.size() 
			|| a.deciPoint != b.deciPoint 
			|| a.sign != b.sign 
			|| a.n != b.n;
	}

	friend bool operator < (InfiniFloat& a, InfiniFloat& b) {
		
		//cout << a.deciPoint << " | " << b.deciPoint << endl;
		//cout << a.n.size() << " | " << b.n.size() << endl;

		if (a == b) {
			return false;
		}

		if (a.isPositive() && b.isNegative()) {
			return false;
		} else if (a.isNegative() && b.isPositive()) {
			return true;
		}

		bool isLess = (a.deciPoint < b.deciPoint) || (a.n.size() < b.n.size());
		long i = 0;

		//cout << "isLess: " << (isLess ? "true" : "false") << endl;
		
		while (
			!isLess 
			&& i <= (long)a.n.size() - 1 
			&& !(a.deciPoint > b.deciPoint) 
			&& !(a.n.size() > b.n.size())
			) {
			/*cout 
				<< endl 
				<< " a[i] = " << a.n[i] 
				<< " | b[i] = " << b.n[i] 
				<< " | less = " << ((a.n[i] < b.n[i]) ? "true" : "false") 
				<< endl;*/
			isLess = (a.n[i] < b.n[i]);
			if (a.n[i] > b.n[i]) {
				break;
			}
			i++;
		}

		return isLess;

	}

	friend bool operator <= (InfiniFloat& a, InfiniFloat& b) {
		return a < b || a == b;
	}

	friend bool operator > (InfiniFloat& a, InfiniFloat& b) {
		return !(a < b || a == b);
	}

	friend bool operator >= (InfiniFloat& a, InfiniFloat& b) {
		return !(a < b) || a == b;
	}
	
};

InfiniFloat::InfiniFloat() {
	this -> deciPoint = 0;
}
InfiniFloat::InfiniFloat(long input) {
	if (input == 0) {
		this -> n.push_back(0);
	} else {
		this -> n = vectorize(abs(input));
	}
	this -> deciPoint = this -> n.size();
	this -> sign = input >= 0;
}
InfiniFloat::InfiniFloat(string input) {
	if (input.find_first_of('-') != string::npos && input.find_first_of('-') == 0) {
		input.erase(0, 1);
		this -> sign = false;
	} else {
		this -> sign = true;
	}
	if (input.find_first_of('.') != string::npos) {
		this -> deciPoint = input.find_first_of('.');
		input.erase(input.find_first_of('.'), 1);
	} else {
		this -> deciPoint = input.length();
	}
	if (input == "0") {
		this -> n.push_back(0);
	} else {
		this -> n = vectorize(input);
		if (this -> n.empty()) {
			this -> n.push_back(0);
		}
	}
	(*this).trim();
}

vector<short> InfiniFloat::vectorize(long input) {
		
	vector<short> n;

	while (input > 0) {
		n.insert(n.begin(), input % 10);
		input /= 10;
	}

	return n;

}
vector<short> InfiniFloat::vectorize(string input) {
		
	vector<short> n;
	string temp;

	for (long i = 1; i <= (long)input.length(); i++) {
		temp = input.substr(input.length() - i, 1);
		try {
			n.insert(n.begin(), stoi(temp));
		} catch (invalid_argument&) {
			cout << "Error: invalid character in \"" << input << "\"" << endl;
			n.erase(n.begin(), n.end());
			return n;
		}
	}

	return n;

}

void InfiniFloat::trim() {
	
	//cout << "trim1" << endl;

	while (this -> n.front() == 0 && this-> deciPoint > 1) {
		this -> n.erase(this -> n.begin());
		this -> deciPoint--;
	}

	//cout << "trim2" << endl;

	while (this -> n.back() - 1 == 0 && (long)this -> n.size() > 1) {
		this -> n.erase(this -> n.end());
	}

	//cout << "trim3" << endl;

}

void InfiniFloat::flipSign() {
	this -> sign = !this -> sign;
}

void InfiniFloat::align(InfiniFloat& a, InfiniFloat& b) {
	
	InfiniFloat *c = (a >= b) ? &a : &b;		// the larger of the two numbers
	InfiniFloat *d = (a < b) ? &a : &b;			// the smaller of the two numbers
	/*cout 
		<< "align" 
		<< " | c.deciPoint = " << (*c).deciPoint 
		<< " | d.deciPoint = " << (*d).deciPoint
		<< endl;*/
	//cout << "c: " << *c << " | d: " << *d << endl;
	while ((*d).deciPoint < (*c).deciPoint) {	// fill beginning of smaller number
		(*d).n.insert((*d).n.begin(), 0);
		(*d).deciPoint++;
		//cout << "c: " << *c << " | d: " << *d << endl;
	}
	//cout << "align2" << endl;
	//cout << "---------------------------" << endl;
	while ((*d).n.size() < (*c).n.size()) {		// fill smaller number until end of larger number
		(*d).n.push_back(0);
		//cout << "c: " << *c << " | d: " << *d << endl;
	}
	//cout << "end" << endl;

}

bool InfiniFloat::isPositive() {
	return this -> sign;
}
bool InfiniFloat::isNegative() {
	return !this -> sign;
}

void InfiniFloat::swap(InfiniFloat& a, InfiniFloat& b) {
	InfiniFloat temp = a;
	a = b;
	b = temp;
}
void InfiniFloat::swap(InfiniFloat* a, InfiniFloat* b) {
	InfiniFloat *temp = a;
	a = b;
	b = temp;
	delete(temp);
}

InfiniFloat InfiniFloat::zero() {
	return *(new InfiniFloat(0));
}
InfiniFloat InfiniFloat::one() {
	return *(new InfiniFloat(1));
}