#include <iostream>
#include <ctime>
#include <chrono>
#include "infiniFloat.h"
using namespace std;

int main() {

	InfiniFloat a = "12.34";
	InfiniFloat b = "-5.6";
	//InfiniFloat add;
	//InfiniFloat *c = new InfiniFloat("0.5");
	//InfiniFloat d;
	//cout << "Enter a number: ";
	//cin >> d;
	//InfiniFloat::swap(a, b);
	InfiniFloat sum = a + b;
	InfiniFloat diff = a - b;
	InfiniFloat product = a * b;
	InfiniFloat quotient = a / b;
	
	cout 
		<< "a = " << a << endl
		<< "b = " << b << endl
		//<< "c = " << *c << endl
		//<< "d = " << d << endl
		<< "a == b = " << ((a == b) ? "true" : "false") << endl
		<< "a != b = " << ((a != b) ? "true" : "false") << endl
		<< "a < b = " << ((a < b) ? "true" : "false") << endl
		<< "a <= b = " << ((a <= b) ? "true" : "false") << endl
		<< "a > b = " << ((a > b) ? "true" : "false") << endl
		<< "a >= b = " << ((a >= b) ? "true" : "false") << endl
		<< "a + b = " << sum << endl
		<< "a - b = " << diff << endl
		<< "a * b = " << product << endl
		<< "a = " << a << endl
		<< "b = " << b << endl
		<< endl;
	cout
		<< "a++ = " << a++ << endl 
		<< "b-- = " << b-- << endl  
		<< endl;

	/*auto t_start = chrono::high_resolution_clock::now();

	add = a * b;

	auto t_end = std::chrono::high_resolution_clock::now();

	cout 
		<< "c = " << add << " | " 
		<< chrono::duration<double, milli>(t_end-t_start).count()
		<< endl;*/

	return 0;
	
}